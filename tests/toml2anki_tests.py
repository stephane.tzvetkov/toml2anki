import inspect
import pytest

from toml2anki.toml2anki import main

# TODO
# - test if bad input file location
# - test if bad ouput dir location
# - test if bad tmp build dir location
# - test if bad media dir location
# - test if with empty notetype
# - test if with no deck name 
# - test if with empty deck name 
# - test different note types and associated note types fields

@pytest.mark.parametrize("option", [
    ("--input-toml-file", "./tests/toml-tests-decks/basic_valid_deck.toml",
    "--media-directory", "",
    "--tmp-build-directory", "./tests/temp-tests-builds",
    "--output-directory", "./tests/temp-tests-outputs",)
    ])
def test_multiple_valid_notes(option):
    try:
        main(option)
    except Exception:
        pytest.fail("Unexpected MyError ..")

@pytest.mark.parametrize("option", ["--version"])
def test_version(capsys, option):
    try:
        main([option])
    except SystemExit:
        pass
    output = capsys.readouterr().out
    assert "toml2anki 0.1.0\n" == output

@pytest.mark.parametrize("option", [("--help"), ("-h")])
def test_help(capsys, option):
    try:
        main([option])
    except SystemExit:
        pass
    output = capsys.readouterr().out

    #assert """usage: toml2anki [-h] --input-toml-file INPUT_TOML_FILE
    #             [--media-directory INPUT_MEDIA_DIRECTORY]
    #             [--tmp-build-directory TMP_BUILD_DIRECTORY]
    #             [--output-directory OUTPUT_DIRECTORY] [--version]""" in output

    assert ("usage: toml2anki [-h] --input-toml-file INPUT_TOML_FILE [--media-directory "
            "MEDIA_DIRECTORY]\n"
            "                 [--tmp-build-directory TMP_BUILD_DIRECTORY] [--output-directory "
            "OUTPUT_DIRECTORY]\n"
            "                 [--verbose] [--version]\n") in output
    assert inspect.cleandoc("""toml2anki is a simple Python project based on
                 [ki](https://github.com/langfield/ki), that will let you create
                 [Anki](https://apps.ankiweb.net/) decks from simple
                 [TOML](https://toml.io/en/) files.""") in output

#def main() -> None:
#    result = main_toml2anki(["--input-toml-file", "./toml_tests_decks/basic_valid_deck.toml",
#                    "--media-directory", "",
#                    "--tmp-build-directory", "/tmp/build",
#                    "--output-directory", "./tests-outputs",
#                    ])
