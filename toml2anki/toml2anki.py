import argparse
import json
import os
import pathlib
import shutil
import sys
import tomllib

from uuid import UUID
from contextlib import suppress
from loguru import logger

import ki


VERSION:str="0.1.0"
GUIDS:list[str]=[]


@logger.catch
def run_ki_compile(
        build_dir:str,
        deck_name:str,
        ) -> None:
    """
    Run the ki `compile.py` script in order to generate the final .apkg file (Anki deck).

    :param build_dir: str: the path to the build directory
    :param deck_name: str: the name of the deck to create
    :raises exception: Exception: thrown if the ki `compile.py` script did not execute correctly
    """

    logger.trace("Running ki `compile.py`...")
    # ⚠️  TODO / FIXME: Next line is a hacky way to use the `compile.py` script in the ki module.
    #                  At the moment I didn't found a way to call it otherwise.
    result = os.system(
            f"python3 {os.path.dirname(ki.__file__)}/../../../../src/ki/compile.py "
            f"--collection {build_dir}/collection.anki2 "
            f"--deck '{deck_name}'"
            )
    if result != 0:
        raise Exception(f"The ki compile command did not returned 0 "
                        f"(returned {result} instead)!")
    logger.trace("Ki `compile.py` ran!")


@logger.catch
def change_directory_to_output_directory(
        output_dir:str,
        ) -> None:
    """
    Change directory to the output directory before creating the .apkg file (Anki deck) that will
    end up here.

    :param output_dir: str: the path to the output directory
    """

    logger.trace(f"Changing directory to `{output_dir}`...")
    os.chdir(output_dir)
    logger.trace("Directory changed!")


@logger.catch
def absolute_output_directory_path(
        output_dir:str,
        current_dir:str,
        ) -> str:
    """
    Find the absolute path of the outuput directory if it is relative and not already absolute.

    :param output_dir: str: the path to the output directory
    :param current_dir: str: the absolute path to the current directory
    :returns: str: the absolute path to the output directory
    """

    logger.trace("Making sure that the output directory is an absolute path...")
    if not os.path.isabs(output_dir):
        output_dir = os.path.normpath(os.path.join(current_dir, output_dir))
    logger.debug(f"Output directory absolute location: {output_dir}")
    logger.trace("Output directory is absolute!")
    return output_dir


@logger.catch
def absolute_build_directory_path(
        build_dir:str,
        current_dir:str,
        ) -> str:
    """
    Find the absolute path of the build directory if it is relative and not already absolute.

    :param build_dir: str: the path to the build directory
    :param current_dir: str: the absolute path to the current directory
    :returns: str: the absolute path to the build directory
    """

    logger.trace("Making sure that the temporary build directory is an absolute path...")
    if not os.path.isabs(build_dir):
        build_dir = os.path.normpath(os.path.join(current_dir, build_dir))
    logger.debug(f"Temporary build directory absolute location: {build_dir}")
    logger.trace("Temporary build directory is absolute!")
    return build_dir


@logger.catch
def run_ki_pull_and_ki_push() -> None:
    """
    Run `ki pull` (see https://langfield.github.io/ki/#pull) 
    and then run `ki push` (see https://langfield.github.io/ki/#push)
    """

    logger.trace("Running `ki pull` and `ki push`...")
    sys.argv = [sys.argv[0]]  # clear args
    with suppress(SystemExit):
        ki.pull()
    with suppress(SystemExit):
        ki.push()
    logger.trace("`ki pull` and `ki push` ran!")
    #ki.check_fields_health() # TODO?


@logger.catch
def change_directory_to_collection(
        build_dir:str,
        ) -> None:
    """
    Change directory to the collection directory.

    :param build_dir: str: the path to the build directory
    """

    logger.trace(f"Changing directory to `{build_dir}/collection`...")
    os.chdir(f"{build_dir}/collection")
    logger.trace("Directory changed!")


@logger.catch
def move_ki_deck_inside_collection(
        build_dir:str,
        deck_name:str,
        ) -> None:
    """
    Move the ki deck (ki markdown files) inside the collection directory.

    :param build_dir: str: the path to the build directory
    :param deck_name: str: the name of the deck to create
    """

    logger.trace(f"Moving `{build_dir}/{deck_name}` into `{build_dir}/collection/`...")
    shutil.move(f"{build_dir}/{deck_name}",
                f"{build_dir}/collection/")
    logger.trace(f"`{deck_name}` moved!")


@logger.catch
def run_ki_clone(
        build_dir:str,
        ) -> None:
    """
    Run `ki clone` (see https://langfield.github.io/ki/#clone).

    :param build_dir: str: the path to the build directory
    """

    logger.trace("Running `ki clone`...")
    with suppress(SystemExit):
        ki.clone([f"{build_dir}/collection.anki2",
                  f"{build_dir}/collection"])
    logger.trace("`ki clone` ran!")


@logger.catch
def create_collection_anki2_file(
        build_dir:str,
        ) -> None:
    """
    Create the `collection.anki2` file inside the build directory.

    :param build_dir: str: the path to the build directory
    """

    logger.trace(f"Creating `{build_dir}/collection.anki2`...")
    open(f"{build_dir}/collection.anki2", "x")
    logger.trace(f"`{build_dir}/collection.anki2` created!")


@logger.catch
def create_anki_deck(
        build_dir:str,
        current_dir:str,
        deck_name:str,
        output_dir:str
        ) -> None:
    """
    Create the Anki deck (.apkg file) based on the previously generated ki deck (.md files).

    :param build_dir: str: the path to the build directory
    :param current_dir: str: the absolute path to the current directory
    :param deck_name: str: the name of the deck to create
    :param output_dir: str: the path to the output directory
    """

    create_collection_anki2_file(build_dir)
    run_ki_clone(build_dir)
    move_ki_deck_inside_collection(build_dir, deck_name)
    change_directory_to_collection(build_dir)
    run_ki_pull_and_ki_push()
    build_dir = absolute_build_directory_path(build_dir, current_dir)
    output_dir = absolute_output_directory_path(output_dir, current_dir)
    change_directory_to_output_directory(output_dir)
    run_ki_compile(build_dir, deck_name)
    logger.success("Anki deck created!")


@logger.catch
def write_markdown_note_to_file(
        build_dir:str,
        deck_name:str,
        guid:str,
        markdown_note:str,
        ) -> None:
    """
    Write a ki markdown note to a file inside the build directory.

    :param build_dir: str: the path to the build directory
    :param deck_name: str: the name of the deck to create
    :param guid: str: the note's GUID
    :param markdown_note: str: the content of the ki markdown note
    """

    with open(f"{build_dir}/{deck_name}/note_{guid}.md", 'w') as file:
        file.write(markdown_note)
    pass


@logger.catch
def create_markdown_note(
        guid:str,
        notetype:str,
        tags:str,
        note:dict,
        ) -> str:
    """
    Create a ki Markdown note based on the TOML note and write it to a file inside the build
    directory.

    :param guid: str: the already extracted note's GUID
    :param notetype: str: the already extracted note's type (can be a custom type)
    :param tags: str: the already extracted note's tags (if any)
    :param note: dict: the note's full content
    :returns: str: the ki Markdown note
    """

    # TODO: check notetype fields

    markdown_note=(f"# Note\n"
                   f"```\n"
                   f"guid: {guid}\n"
                   f"notetype: {notetype}\n"
                   f"```\n"
                   f"\n"
                   f"### Tags\n"
                   f"```{tags}\n"
                   f"```\n")

    for key, value in note.items():
        if key not in ["guid", "notetype", "tags"]:
            if key == "front":
                markdown_note += (f"\n"
                                  f"## Front\n"
                                  f"{value}\n")
            elif key == "back":
                markdown_note += (f"\n"
                                  f"## Back\n"
                                  f"{value}\n")
            else:
                markdown_note += (f"\n"
                                  f"## {key}\n"
                                  f"{value}\n")

    return markdown_note


@logger.catch
def get_notetype_from_note(
        note:dict,
        guid:str,
        ) -> str:
    """
    Retrieve the note's type from the TOML note.

    :param note: dict: the note's full content
    :param guid: str: the already extracted note's GUID
    :returns: str: the note's type
    """

    notetype:str = "Basic"
    if note.get("notetype"):
        notetype = note["notetype"]
        if not notetype:
            logger.warning(f"In note GUID {guid}, the `notetype` field is specified "
                           f"but left empty! the \"Basic\" note type will be assumed!")
            notetype = "Basic"
    return notetype


@logger.catch
def get_tags_from_note(
        note:dict,
        ) -> str:
    """
    Retrieve the note's tag from the TOML note.

    :param note: dict: the note's full content
    :returns: str: the note's tag
    """

    tags:str = ""
    if note.get("tags"):
        tags = note["tags"]
        if tags:
            # remove empty-lines:
            tags = "\n".join([ll.rstrip() for ll in tags.splitlines() if ll.strip()])
            # remove spaces by line-returns:
            tags = tags.replace(" ","\n")
            # make sure to start with an empty-line/line-return:
            tags = "\n"+tags
    return tags


@logger.catch
def get_guid_from_note(
        note:dict,
        key:str,
        ) -> str:
    """
    Retrieve the note's GUID from the TOML note.

    :param note: dict: the note's full content
    :param key: str: the identifier of the note inside the TOML file.
    :returns: str: the note's GUID
    """

    guid:str = key
    if note.get("guid"):
        guid = note["guid"]

        if not guid:
            raise Exception(f"A note has no GUID: {note}")

        if guid in GUIDS:
            raise Exception(f"Two notes have the same GUID: {guid}")

    if not is_valid_uuid(guid):
        logger.error(f"The GUID {guid} is not \"valid\", "
                       f"as specified by RFC 4122."
                       f"You should use a dedicated tool to generate it "
                       f"(e.g. `uuidgen` on GNU/Linux).\n\n"
                       f"This is REALLY important because you might end up "
                       f"with duplicated GUIDs accross multiple decks, "
                       f"that could cause notes to override others "
                       f"and messing up your cards!")
        raise Exception(f"Bad GUID: {guid}")

    GUIDS.append(guid)
    return guid


@logger.catch
def is_valid_uuid(
        uuid_to_test:str,
        version:int=4
        ) -> bool:
    """
    Check if a UUID/GUID is valid according to RFC 4122
    (see https://docs.python.org/3/library/uuid.html).

    :param uuid_to_test: str: the UUID/GUID to check
    :param version: str: the associated UUID version
    :returns: bool: `True` if the UUID/GUID is valid, `False` else
    """

    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


@logger.catch
def check_if_item_is_note_or_deck(
        key:str,
        value:str,
        ) -> None:
    """
    Raise a warning when ignoring an item in the input .toml file that is not a note nor a deck
    name.

    :param key: dict: the path to the media directory
    :param value: str: the path to the build directory
    """

    if not is_valid_uuid(key) and key != "deck":
        logger.warning(f"An item has been found in the input .toml file that is not a note "
                       f"nor a deck name. It will be ignored."
                       f"\nItem key: {key}\nItem value: {value}")


@logger.catch
def copy_media_directory_if_any(
        media_directory:str,
        build_dir:str,
        deck_name:str,
        ) -> None:
    """
    Copy the media directory (if any) into the ki deck folder of the build directory.

    :param media_directory: dict: the path to the media directory
    :param build_dir: str: the path to the build directory
    :param deck_name: str: the name of the deck to create
    """

    if media_directory:
        logger.trace(f"Copying the media directory to `{build_dir}/{deck_name}/_media`...")
        shutil.copytree(src = f"{media_directory}", dst = f"{build_dir}/{deck_name}/_media")
        logger.trace("Media directory copied!")


@logger.catch
def empty_tmp_build_dir(
        build_dir:str,
        deck_name:str,
        ) -> None:
    """
    Empty the temporary build directory by removing it and re-creating it.

    :param build_dir: str: the path to the build directory
    :param deck_name: str: the name of the deck to create
    """

    logger.trace(f"Emptying the temporary build directory (`{build_dir}`)...")
    shutil.rmtree(path = build_dir, ignore_errors = True)
    pathlib.Path(f"{build_dir}/{deck_name}").mkdir(parents=True, exist_ok=True)
    logger.trace("Temporary build directory empty!")


@logger.catch
def create_markdown_notes_from_toml_deck(
        deck_name:str,
        notes:dict,
        media_directory:str,
        build_dir:str,
        ) -> None:
    """
    Create ki .md notes from the .toml deck

    :param deck_name: str: the name of the deck to create
    :param note: dict: the note's full content
    :param media_directory: dict: the path to the media directory
    :param build_dir: str: the path to the build directory
    """

    empty_tmp_build_dir(build_dir, deck_name)
    copy_media_directory_if_any(media_directory, build_dir, deck_name)

    logger.trace("Iterating through the notes from the .toml file...")
    for key, value in notes.items():

        check_if_item_is_note_or_deck(key, value)
        if isinstance(value,dict):
            note:dict = value
            guid:str = get_guid_from_note(note, key)
            tags:str = get_tags_from_note(note)
            notetype:str = get_notetype_from_note(note, guid)
            markdown_note = create_markdown_note(guid, notetype, tags, note)
            write_markdown_note_to_file(build_dir, deck_name, guid, markdown_note)

            pretty_note = json.dumps(note, indent=4)
            logger.opt(colors=True).debug(
                    f"- note raw key: \n<normal><white>{key}</white></normal>\n"
                    f"- note raw value: \n<normal><white>{pretty_note}</white></normal>\n"
                    f"- note extracted guid: \n<normal><white>{guid}</white></normal>\n"
                    f"- note extracted tags: <normal><white>{tags}</white></normal>\n"
                    f"- note generated in markdown: "
                    f"\n<normal><white>{markdown_note}</white></normal>"
                    f"- note written to: "
                    f"\n<normal><white>{build_dir}/{deck_name}/note_{guid}.md</white></normal>"
                    )

    logger.trace("Temporary .md notes created!")


@logger.catch
def extract_notes_from_toml_file(
        input_toml_file:str,
        ) -> dict:
    """
    Extract all notes of the input .toml file.

    :param input_toml_file: str: 
    :returns: dict: the extracted notes
    """

    with open(input_toml_file, "rb") as f:
        logger.trace(f"Loading the .toml input file (`{input_toml_file}`)...")
        notes:dict = tomllib.load(f)
    logger.trace(f"The .toml input file (`{input_toml_file}`) has been loaded!")
    return notes


@logger.catch
def get_temporary_output_directory(
        output_dir:str,
        current_dir:str,
        ) -> str:
    """
    Get the output directory and return it if not empty, return the current directory else.
    
    :param output_dir: str: the path to the output directory
    :returns: str: the output directory relative or absolute path
    """

    if not output_dir:
        output_dir= current_dir
        logger.info(f"No output directory has been specified. The current directory "
                    f"(`{current_dir}`) will be used to output the final Anki `*.apkg` file.")
    return output_dir


@logger.catch
def get_temporary_build_directory(
        tmp_build_directory:str,
        ) -> str:
    """
    Get the output directory and return it if not empty, return the current directory else.
    
    :param output_dir: str: the path to the output directory
    :returns: str: the output directory path (relative or absolute)
    """

    if not tmp_build_directory:
        tmp_build_directory = "/tmp/build"
        logger.info("No temporary build directory has been specified. The `/tmp/build` directory "
                    "will be used for temporary builds.")
    return tmp_build_directory


@logger.catch
def configure_logger(
        verbosity:int=0
        ) -> None:
    """
    Configure the logger according to --verbose / -v option.
    
    :param verbosity: int: the path to the output directory
    """

    logger.remove()
    form: str=("\n{level.icon} <green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
               "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> | "
               "<level>{level} ({level.no}):\n{message}</level>\n")
    if verbosity == 0:
        logger.add(sys.stderr, colorize=True, format=form, level="SUCCESS")
    elif verbosity == 1:
        logger.add(sys.stderr, colorize=True, format=form, level="INFO")
    elif verbosity == 2:
        logger.add(sys.stderr, colorize=True, format=form, level="DEBUG")
    elif verbosity >= 3:
        logger.add(sys.stderr, colorize=True, format=form, level="TRACE")
    else:
        raise Exception(f"The `--verbose / -v` is negative? Verbosity = {verbosity}")

@logger.catch
def parse_arguments(
        raw_args=None
        ) -> argparse.Namespace:
    """
    Parse the program arguments and extract the specified options.

    :returns: argparse.Namespace: all the programs arguments
    """
    parser = argparse.ArgumentParser(
            prog="toml2anki",
            description=("toml2anki is a simple Python project based on "
                         "[ki](https://github.com/langfield/ki), that will let you create "
                         "[Anki](https://apps.ankiweb.net/) decks from simple "
                         "[TOML](https://toml.io/en/) files."))
    parser.add_argument("--input-toml-file", "-i", type=str, required=True, help="todo")
    parser.add_argument("--media-directory", "-m", type=str, required=False, help="todo")
    parser.add_argument("--tmp-build-directory", "-b", type=str, required=False, help="todo")
    parser.add_argument("--output-directory", "-o", type=str, required=False, help="todo")
    parser.add_argument("--verbose", "-v", action="count", default=0, required=False, help="todo")
    parser.add_argument("--version", action="version", version=f"toml2anki {VERSION}")
    return parser.parse_args(raw_args)


@logger.catch
def main(
        raw_args=None
        ) -> None:
    """
    Main function running toml2anki
    """
    args: argparse.Namespace = parse_arguments(raw_args)

    input_toml_file:str = args.input_toml_file
    media_directory:str = args.media_directory
    tmp_build_directory:str = args.tmp_build_directory
    output_directory:str = args.output_directory
    verbosity:int = args.verbose
    configure_logger(verbosity)

    logger.debug(f"""Options passed:
    --input-toml-file / -i : {input_toml_file}
    --media-directory / -m : {media_directory}
    --tmp-build-directory / -b : {tmp_build_directory}
    --output-directory / -o : {output_directory}
    --verbose / -v : {verbosity}""")

    tmp_build_directory = get_temporary_build_directory(tmp_build_directory)
    current_dir:str = os.getcwd()
    logger.debug(f"Current directory: {current_dir}")
    output_directory = get_temporary_output_directory(tmp_build_directory, current_dir)
    notes:dict = extract_notes_from_toml_file(input_toml_file)
    deck_name:str = notes["deck"]
    logger.debug(f"Deck name: {deck_name}")
    logger.trace("Creating temporary .md notes from the .toml input...")
    create_markdown_notes_from_toml_deck(deck_name, notes, media_directory, tmp_build_directory)
    logger.trace("Creating .apkg Anki deck from Temporary .md notes...")
    create_anki_deck(tmp_build_directory, current_dir, deck_name, output_directory)


if __name__ == "__main__":
    main()
