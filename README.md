# toml2anki

toml2anki is a very simple Python project based on [ki](https://github.com/langfield/ki), that will
let you create [Anki](https://apps.ankiweb.net/) decks from simple [TOML](https://toml.io/en/)
files.
